Kota Sorong adalah sebuah kota di Provinsi Papua Barat, Indonesia.
Kota ini dikenal dengan sebutan Kota Minyak, di mana Nederlands Nieuw-Guinea Petroleum Maatschappij (NNGPM) mulai melakukan aktivitas pengeboran minyak bumi di Sorong sejak tahun 1935.
Sorong adalah kota terbesar di Provinsi Papua Barat serta kota terbesar kedua di Papua Indonesia, setelah Kota Jayapura.
Kota Sorong sangatlah strategis karena merupakan pintu keluar masuk dan transit ke Provinsi Papua Barat.
Kota Sorong juga merupakan kota industri, perdagangan dan jasa, karena Kota Sorong dikelilingi oleh kabupaten lain yang mempunyai sumber daya alam yang sangat potensial sehingga membuka peluang bagi investor dalam maupun luar negeri untuk menanamkan modalnya.
Nama Sorong berasal dari kata Soren.
Soren dalam bahasa Biak Numfor yang berarti laut yang dalam dan bergelombang.
Kata Soren digunakan pertama kali oleh suku Biak Numfor yang berlayar pada zaman dahulu dengan perahu-perahu layar dari satu pulau ke pulau lain hingga tiba dan menetap di Kepulauan Raja Ampat.
Suku Biak Numfor inilah yang memberi nama "Daratan Maladum " (sekarang termasuk bagian dari wilayah Kota Sorong) dengan sebutan “Soren” yang kemudian dilafalkan oleh para pedagang Tionghoa, misionaris clad Eropa, Maluku dan Sangihe Talaut dengan sebutan Sorong.
Sekitar tahun 1935, pada masa Hindia Belanda, Sorong didirikan sebagai base camp Bataafse Petroleum Maatschappij (BPM) sedangkan pusat pemerintahan didirikan di Pulau Doom.
Setelah penyerahan Irian Barat secara penuh oleh Penguasa Sementara PBB atau UNTEA (United Nations Temporary Executive Authority) kepada pemerintah Republik Indonesia, maka pada tahun 1965 berdasarkan berbagai pertimbangan kemudian diangkat seorang wakil Bupati Koordinator yang berkedudukan di Sorong, dengan tugas: 1.
Mengkoordinir pelaksanaan tugas pemerintahan oleh Kepala Pemerintahan Setempat (KPS) Sorong, Raja Ampat, Teminabuan dan Ayamaru.
2.
Mempersiapkan pemecahan Kota Irian Barat Bagian Barat menjadi 2 (dua) Kota.
Pada tahun 1969, dengan selesainya pelaksanaan Penentuan Pendapat Rakyat (Pepera) maka perkembangan status dari Kota Administratif menjadi Kota Otonom ini tidak ada perubahan dalam pembagian wilayah dan keadaan sampai dengan akhir tahun 1972 adalah sebagai berikut:
Wilayah Pemerintahan Setempat Sorong dengan ibukota Sorong;
Wilayah Pemerintahan Setempat Raja Ampat dengan ibukota Sorong Doom;
Wilayah Pemerintahan Setempat Teminabuan dengan ibukota Teminabuan;
Wilayah Pemerintahan Setempat Ayamaru dengan ibu kota Ayamaru.
Pembagian wilayah di Sorong seperti tersebut di atas berlaku sampai tahun 1973 saat dilakukannya penghapusan wilayah-wilayah Kepala Daerah Setempat dan sejumlah distrik dan dibentuknya Pemerintahan Wilayah Kecamatan Tahap Pertama Tahun 1973-1974.
Kota Sorong pada mulanya merupakan salah satu kecamatan yang dijadikan pusat pemerintahan Kabupaten Sorong.
Namun dalam perkembangannya telah mengalami perubahan sesuai Peraturan Pemerintah No.
31 Tahun 1996 tanggal 3 Juni 1996 menjadi Kota Administratif Sorong.
Selanjutnya berdasarkan Undang-Undang no.
45 Tahun 1999 Kota Administratif Sorong ditingkatkan statusnya menjadi daerah otonom sebagai Kota Sorong
Secara geografis, Kota Sorong berada pada koordinat 131°51' Bujur Timur dan 0° 54' Lintang Selatan, memiliki batas-batas sebagai berikut :
Sebelah Timur : berbatasan dengan Distrik Makbon (Kabupaten Sorong) dan Selat Dampir
Sebelah Barat : berbatasan dengan Selat Dampir
Sebelah Utara : berbatasan dengan Distrik Makbon (Kabupaten Sorong) dan Selat Dampir
Sebelah Selatan : berbatasan dengan Distrik Aimas (Kabupaten Sorong) dan Distrik Salawati (Kabupaten Raja Ampat)
Luas wilayah Kota Sorong mencapai 1.105,00 km2, atau sekitar 1.
13% dari total luas wilayah Papua Barat.
Wilayah kota ini berada pada ketinggian 3 meter dari permukaan laut dan suhu udara minimum di Kota Sorong sekitar 23, 1 °C dan suhu udara maximum sekitar 33, 7 °C.
Curah hujan tercatat 2.911 mm.
Curah hujan cukup merata sepanjang tahun.
Tidak terdapat bulan tanpa hujan, banyaknya hari hujan setiap bulan antara 9 - 27 hari.
Kelembaban udara rata-rata tercatat 84 %.
Keadaan topografi Kota Sorong sangat bervariasi terdiri dari pegunungan, lereng, bukit-bukit dan sebagian adalah dataran rendah, sebelah timur di kelilingi hutan lebat yang merupakan hutan lindung dan hutan wisata.
Keadaan geologi Kota Sorong terdapat hamparan galian golongan C seperti batu gunung, batu kaIi, sirtu, pasir, tanah uruk dan kerikil.
Sedangkan jenis tanah yang terdapat di Kota Sorong adalah tanah latosal putih yang terdapat di pinggiran pantai Tanjung Kasuari dan tanah fudsolik merah kuning yang terdapat dihamparan seluruh kawasan Distrik Sorong Timur.
Keadaan permukaan Kota Sorong yang terdiri dari gunung, buki-bukit dan dataran yang rendah yang ditandai dengan jurang, dan wilayah ini dialiri sungai-sungai sedang, kecil seperti sungai Rufei, sungai Klabala, sungai Duyung, sungai Remu, sungai Klagison, sungai Klawiki, sungai Klasaman dan sungai Klabtin.
Berdasarkan hasil Pencacahan Sensus Penduduk 2010, jumlah penduduk Kota Sorong (Angka Sementara) adalah 190.341 jiwa, yang terdiri atas 99.898 laki-laki dan 90.446 perempuan.
Jumlah penduduk terbanyak di Distrik Sorong Utara sebanyak 44.774 jiwa dan jumlah penduduk terkecil berada di Distrik Sorong Kepulauan dengan Jumlah penduduk 9.710 jiwa.
Perbandingan laki-laki dan perempuan atau sex ratio di Kota Sorong adalah sebesar 110,45 persen.
Dari enam distrik yang ada di Kota Sorong, angka Sex Ratio tertinggi berada di Distrik Sorong Timur yaitu sebesar 114,97 persen.
Laju pertumbuhan penduduk Kota Sorong sebesar 7,02 persen per tahun.
Distrik yang laju pertumbuhan penduduknya tertinggi adalah Distrik sorong Timur yakni 14,07 persen dan yang terendah adalah Distrik Sorong Kepulauan yakni sebesar 3,54 persen.
Dengan Luas wilayah 1.105 km² yang didiami penduduk 190.341 jiwa, maka rata-rata tingkat kepadatan penduduk Kota Sorong adalah sebesar 91 jiwa/km2.
Kecamatan yang paling tinggi tingkat kepadatannya adalah Distrik Sorong Manoi yakni sebesar 313 jiwa/km², sedangkan yang paling terendah Distrik Sorong Kepulauan yakni 49 jiwa/km².
Berdasarkan Sensus Penduduk tahun 2015, persentase agama penduduk Kota Sorong adalah Kristen Protestan 55.
31%, Islam 35.
48%, Katolik 8.
22%, Budha 0.
71% dan Hindu 0.
28%.
Secara administratif, Kota Sorong terdiri dari 10 distrik (setingkat dengan kecamatan), yaitu Sorong, Sorong Barat, Sorong Kepulauan, Sorong Timur, Sorong Utara, Sorong Manoi, Sorong Kota, Malaimsimsa, Klaurung dan Maladom Mes.
Kemudian dibagi lagi atas 41 kelurahan yang tersebar pada masing-masing distrik tersebut.
Komoditi unggulan Kota Sorong yaitu sektor pertanian, Perkebunan dan jasa.
Sub sektor perkebunan komoditi yang diunggulkan berupa Kakao, Kelapa dan cengkeh.
Pariwisatanya yaitu wisata alam, wisata adat dan budaya.
Sebagai penunjang kegiatan perekonomian, di provinsi ini tersedia 1 pelabuhan, yaitu Pelabuhan Sorong (Port of Sorong) dan 1 bandar udara, yaitu Bandar Udara Domine Eduard Osok.
Sebelum adanya Bandar Udara Domine Eduard Osok, Kota Sorong menggunakan Bandar Udara Jeffman di Pulau Jeffman.
Untuk mencapai bandar udara tersebut penumpang pesawat terbang menggunakan angkutan kapal dari Kota Sorong.
Saat ini bandar udara tersebut sudah tidak digunakan lagi.
Produk Domestik Regional Bruto (PDRB) Kota Sorong dari tahun ke tahun terus mengalami peningkatan.
Pada tahun 2012 nilainya telah mencapai sekitar 4.206.112,83 juta rupiah atau mengalami peningkatan sebesar 14,43 persen dibandingkan tahun 2011.
Besar kecilnya perkembangan PDRB Kota Sorong berpengaruh terhadap besar kecilnya sumbangan PDRB Kota Sorong terhadap pembentukan PDRB Provinsi Papua Barat.
sumber : sorongkota.
bps.
go.
id
Sektor industri pengolahan merupakan salah satu sektor yang memberikan kontribusi terbesar dalam pembangunan perekonomian Kota Sorong.
Kota Sorong terkenal sebagai salah satu kota dengan peninggalan sejarah bekas perusahaan minyak milik Belanda Heritage Nederlands Nieuw-Guinea Petroleum Maatschappij (NNGPM).
Beberapa kawasan wisata lainnya adalah taman rekreasi pantai Tanjung Kasuari dengan pesona pasir putihnya, termasuk kawasan pantai pada Pulau Raam, Pulau Soop, Pulau Item dan Pulau Doom.
Fasilitas wisata lainnya adalah taman rekreasi pantai Tanjung Kasuari dengan pesona pasir putihnya, Pulau Raam, Pulau Soop dan Pulau Doom yang terkenal dengan pantainya yang indah.
Juga pulau Dofior yang terdapat Tugu Selamat Datang di Kota Sorong dengan menggunakan bahasa Moi (suku asli di Kota Sorong) yang ramah dan bersahabat menyambut pengunjung yang datang di Kota Sorong.
Juga tembok Dofior yang terkenal dengan pemandangan panorama laut dan keindahan alam menjelang senja.
Baubau, Indonesia
Baltimore, Amerika Serikat
Busan, Korea Selatan
Dili, Timor Leste
Fukuoka, Jepang
Singapura, Singapura
Tual, Indonesia
Wisata Kota Sorong
