from __future__ import print_function
import re
import shutil
import codecs
import sys
import string
from nltk.tag import CRFTagger
from zeep import Client
import xml.etree.ElementTree as ET

reload(sys)  
sys.setdefaultencoding('utf8')

src = "C:/temp_skripsi/skripsi/pywikibot/"
dest = "C:/temp_skripsi/skripsi/pywikibot/cultural_heritage/"
dest_kab_kota = "C:/temp_skripsi/skripsi/pywikibot/cultural_heritage_kab_kota/"

kategori_developedIn = [0]
kategori_origin = [1, 5, 13, 15, 16, 11, 18, 19, 21, 25, 26, 27, 28, 31, 32]
kategori_spokenIn = [2]
kategori_location = [3, 4, 6, 8, 10, 12, 14, 22, 24, 29]
kategori_locationOfDiscovery = [17, 7, 20, 23]
kategori_populationPlace = [9, 30]

f_filtering = open("cek_filtering.txt", "w")

def getPredicateObjectCity(kategori, kabupaten_kota):
	if kategori in kategori_developedIn:
		return "bkb:developedInCity bkbr:" + kabupaten_kota.replace(" ", "_")
	elif kategori in kategori_origin:
		return "bkb:originFromCity bkbr:" + kabupaten_kota.replace(" ", "_")
	elif kategori in kategori_spokenIn:
		return "bkb:spokenInCity bkbr:" + kabupaten_kota.replace(" ", "_")
	elif kategori in kategori_location:
		return "bkb:locationInCity bkbr:" + kabupaten_kota.replace(" ", "_")
	elif kategori in kategori_locationOfDiscovery:
		return "bkb:cityOfDiscovery bkbr:" + kabupaten_kota.replace(" ", "_")
	elif kategori in kategori_populationPlace:
		return "bkb:populationPlaceInCity bkbr:" + kabupaten_kota.replace(" ", "_")
	else:
		return "None"

def getPredicateObjectProvince(kategori, provinsi):
	if kategori in kategori_developedIn:
		return "bkb:developedInProvince bkbr:" + provinsi.replace(" ", "_")
	elif kategori in kategori_origin:
		return "bkb:originFromProvince bkbr:" + provinsi.replace(" ", "_")
	elif kategori in kategori_spokenIn:
		return "bkb:spokenInProvince bkbr:" + provinsi.replace(" ", "_")
	elif kategori in kategori_location:
		return "bkb:locationInProvince bkbr:" + provinsi.replace(" ", "_")
	elif kategori in kategori_locationOfDiscovery:
		return "bkb:provinceOfDiscovery bkbr:" + provinsi.replace(" ", "_")
	elif kategori in kategori_populationPlace:
		return "bkb:populationPlaceInProvince bkbr:" + provinsi.replace(" ", "_")
	else:
		return "None"

def getURIKategori(kategori):
	if kategori == 0:
		return "AksaraDaerah"
	elif kategori == 1:
		return "AlatMusikTradisional"
	elif kategori == 2:
		return "BahasaDaerah"
	elif kategori == 3:
		return "Benteng"
	elif kategori == 4:
		return "Candi"
	elif kategori == 5:
		return "CeritaRakyat"
	elif kategori == 6:
		return "Istana"
	elif kategori == 7:
		return "Kerajaan"
	elif kategori == 8:
		return "Kolam"
	elif kategori == 9:
		return "KomunitasAdat"
	elif kategori == 10:
		return "Kuil"
	elif kategori == 11:
		return "LaguTradisional"
	elif kategori == 12:
		return "Makam"
	elif kategori == 13:
		return "MakananMinumanTradisional"
	elif kategori == 14:
		return "Masjid"
	elif kategori == 15:
		return "MotifKain"
	elif kategori == 16:
		return "Musik"
	elif kategori == 17:
		return "NaskahKunoManuskrip"
	elif kategori == 18:
		return "Ornamen"
	elif kategori == 19:
		return "PakaianTradisional"
	elif kategori == 20:
		return "PatungArca"
	elif kategori == 21:
		return "PermainanRakyatTradisional"
	elif kategori == 22:
		return "Petirtaan"
	elif kategori == 23:
		return "Prasasti"
	elif kategori == 24:
		return "Pura"
	elif kategori == 25:
		return "Ritual"
	elif kategori == 26:
		return "RumahTradisional"
	elif kategori == 27:
		return "SeniPertunjukkan"
	elif kategori == 28:
		return "SenjataTradisional"
	elif kategori == 29:
		return "SitusPrasejarah"
	elif kategori == 30:
		return "SukuBangsa"
	elif kategori == 31:
		return "TarianTradisional"
	elif kategori == 32:
		return "TradisiLisan"
	else:
		return "None"

def contain_punctuation(value):
    for c in value:
        if c in string.punctuation and c != "-" and c != "\'" and c != "&":
            return True
    return False

def filtering(arr_entity):
	arr_clean_entity = []
	for i in range(len(arr_entity)):
		temp = arr_entity[i].split(" | ")
		entity = temp[0]
		provinsi = temp[1]
		kabupaten_kota = temp[2]
		#menghilangkan entitas tidak valid provinsi
		entity = re.sub('^provinsi.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kabupaten
		entity = re.sub('^kabupaten.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kota
		entity = re.sub('^kota.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kecamatan
		entity = re.sub('^kecamatan.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid wilayah
		entity = re.sub('^wilayah.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid jalan
		entity = re.sub('^jalan.*', '', entity, flags=re.IGNORECASE)

		#menghilangkan entitas tidak valid kolam renang
		entity = re.sub('.*renang.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kolam ranang
		entity = re.sub('.*ranang.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kolam pemancingan
		entity = re.sub('.*pemancingan.*', '', entity, flags=re.IGNORECASE)

		#menghilangkan entitas tidak valid menara
		entity = re.sub('^menara.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid gereja
		entity = re.sub('.*gereja.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid gedung
		entity = re.sub('.*gedung.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid stasiun
		entity = re.sub('.*stasiun.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid kantor
		entity = re.sub('.*kantor.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid vihara
		entity = re.sub('.*vihara.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid hotel
		entity = re.sub('.*hotel.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid plaza
		entity = re.sub('.*plaza.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid pusat
		entity = re.sub('.*pusat.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid mall
		entity = re.sub('.*mall.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid square
		entity = re.sub('.*square.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid store
		entity = re.sub('.*store.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid trade
		entity = re.sub('.*trade.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid junction
		entity = re.sub('.*junction.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid walk
		entity = re.sub('.*walk.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid yayasan
		entity = re.sub('.*yayasan.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid court
		entity = re.sub('.*court.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid katedral
		entity = re.sub('.*katedral.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid inn
		entity = re.sub('.*inn.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid grand
		entity = re.sub('.*grand.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid restoran
		entity = re.sub('.*restoran.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid PT
		entity = re.sub('.*PT.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid RS
		entity = re.sub('.*RS.*', '', entity, flags=re.IGNORECASE)
		entity = re.sub('.*rumah sakit.*', '', entity, flags=re.IGNORECASE)

		#menghilangkan entitas tidak valid indonesia
		entity = re.sub('.indonesia.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid inggris
		entity = re.sub('.*inggris.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid jepang
		entity = re.sub('.*jepang.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid tionghoa
		entity = re.sub('.*tionghoa.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid india
		entity = re.sub('.*india.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid arab
		entity = re.sub('.*arab.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid cina
		entity = re.sub('.*cina.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid portugis
		entity = re.sub('.*portugis.*', '', entity, flags=re.IGNORECASE)

		#menghilangkan entitas tidak valid sumber
		entity = re.sub('^sumber$', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid total
		entity = re.sub('.*total.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid lainnya
		entity = re.sub('.*lain.*', '', entity, flags=re.IGNORECASE)
		#menghilangkan entitas tidak valid catatan
		entity = re.sub('.*catatan.*', '', entity, flags=re.IGNORECASE)

		if len(entity) > 1 and not contain_punctuation(entity):
			arr_clean_entity.append(entity + " | " + provinsi + " | " + kabupaten_kota)
	return arr_clean_entity

def pattern_matching(arr_entity):
	arr_clean_entity = []
	for i in range(len(arr_entity)):
		temp = arr_entity[i].split(" | ")
		entity = temp[0]
		provinsi = temp[1]
		kabupaten_kota = temp[2]

		#menghilangkan atribut di dalam kurung
		if re.match(r'.+\(.+\).*', entity):		
			entity = re.sub('\s\(.+\)', '', entity).strip()
			entity = re.sub('\(.+\)', '', entity).strip()

		if re.match(r'.+\(.+', entity):
			entity = re.sub('\(.+', '', entity).strip()

		#menghilangkan atribut setelah tanda ;
		if re.match(r'.+\;.*', entity):
			entity = re.sub('\;.*', '', entity).strip()

		#menghilangkan atribut setelah tanda koma (hanya memiliki satu tanda koma)
		if entity.count(',') == 1:
			entity = re.sub('\,.*', '', entity).strip()

		#menghilangkan atribut setelah tanda titik dua
		if re.match(r'.+\:.*', entity):
			entity = re.sub('\:.+', '', entity).strip()

		#menghilangkan atribut setelah tanda -
		if re.match(r'.+\s\-.*', entity):
			entity = re.sub('\-.+', '', entity).strip()

		if re.match(r'.+\-\s.+', entity):
			entity = re.sub('\-.+', '', entity).strip()

		#menghilangkan atribut setelah tanda double spasi
		if re.match(r'.+\s\s.+', entity):
			entity = re.sub('\s\s.+', '', entity).strip()

		#menghilangkan atribut alias
		if re.match(r'.+\s(atau)\s.*', entity):
			entity = re.sub('\s(atau)\s.+', '', entity).strip()

		if re.match(r'.+\/.*', entity):
			entity = re.sub('\/.+', '', entity).strip()

		#menghilangkan atribut setelah kata adalah
		if re.match(r'.+\s(adalah)\s.*', entity):
			entity = re.sub('(adalah)\s.*', '', entity).strip()

		#menghilangkan atribut setelah kata merupakan
		if re.match(r'.+\s(merupakan)\s.*', entity):
			entity = re.sub('\s(merupakan)\s.*', '', entity).strip()

		#menghilangkan atribut setelah kata asli
		if re.match(r'.+\s(asli)\s.+', entity):
			entity = re.sub('(asli)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata khas
		if re.match(r'.+\s(khas)\s.+', entity):
			entity = re.sub('(khas)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata yang
		if re.match(r'.+\s(yang)\s.+', entity):
			entity = re.sub('(yang)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata yaitu
		if re.match(r'.+\s(yaitu).+', entity):
			entity = re.sub('\s(yaitu).+', '', entity).strip()
		
		#menghilangkan atribut setelah kata depan di
		if re.match(r'.+\s(di)\s.+', entity):
			entity = re.sub('\s(di)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata depan oleh
		if re.match(r'.+\s(oleh)\s.+', entity):
			entity = re.sub('\s(oleh)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata depan dari
		if re.match(r'.+\s(dari)\s.+', entity, re.I):
			entity = re.sub('(dari)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata depan pada
		if re.match(r'.+\s(pada)\s.+', entity):
			entity = re.sub('(pada)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata dengan
		if re.match(r'.+\s(dengan)\s.+', entity):
			entity = re.sub('(dengan)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata secara
		if re.match(r'.+\s(secara)\s.+', entity):
			entity = re.sub('\s(secara)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata sudah
		if re.match(r'.+\s(sudah)\s.+', entity):
			entity = re.sub('\s(sudah)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata terbagi
		if re.match(r'.+\s(terbagi)\s.+', entity):
			parts = re.split(' terbagi ', entity, 1)
			left = parts[0].strip()
			arr_clean_entity.append(left + " | " + provinsi + " | " + kabupaten_kota)

		#menghilangkan atribut setelah kata bentuknya
		if re.match(r'.+\s(bentuknya)\s.+', entity):
			entity = re.sub('\s(bentuknya)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata berakhir
		if re.match(r'.+\s(berakhir)\s.+', entity):
			entity = re.sub('\s(berakhir)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata berbahan
		if re.match(r'.+\s(berbahan)\s.+', entity):
			entity = re.sub('\s(berbahan)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata dirayakan
		if re.match(r'.+\s(dirayakan)\s.+', entity):
			entity = re.sub('\s(dirayakan)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata mendiami
		if re.match(r'.+\s(mendiami)\s.+', entity):
			entity = re.sub('\s(mendiami)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata berdiam
		if re.match(r'.+\s(berdiam).*', entity):
			entity = re.sub('\s(berdiam).*', '', entity).strip()

		#menghilangkan atribut setelah kata sejenis
		if re.match(r'.+\s(sejenis)\s.+', entity):
			entity = re.sub('\s(sejenis)\s.+', '', entity).strip()

		#menghilangkan atribut setelah kata tersebar
		if re.match(r'.+\s(tersebar).*', entity):
			entity = re.sub('\s(tersebar).*', '', entity).strip()

		#menghilangkan setelah , dipakai
		if re.match(r'.+\,\s(dipakai).*', entity):
			entity = re.sub('\,\s(dipakai).*', '', entity).strip()

		#menghilangkan setelah dituturkan
		if re.match(r'.+\s(dituturkan).*', entity):
			entity = re.sub('\s(dituturkan).*', '', entity).strip()

		#menghilangkan setelah dituturkan
		if re.match(r'.+\s(artinya).*', entity):
			entity = re.sub('\s(artinya).*', '', entity).strip()

		if re.match(r'.+\s(meliputi)\s.+', entity):
			entity = re.sub('\s(meliputi).+', '', entity).strip()

		#mengambil kumpulan entitas yang dipisahkan dengan tanda koma
		if re.match(r'((.+)\,\s)+(dan)\s.+', entity):
			#menghilangkan kata dan	
			entity = re.sub('dan\s', '', entity).strip()
			#split berdasarkan tanda koma
			parts = re.split(',\s', entity)
			#ambil awalan token pertama
			awalan = parts[0].split(' ')[0]
			count = 0
			for elem in parts:
				if (count > 0):
					arr_clean_entity.append(awalan + " " + elem + " | " + provinsi + " | " + kabupaten_kota)
				else:
					arr_clean_entity.append(elem + " | " + provinsi + " | " + kabupaten_kota)
				count = count + 1
			entity = ""

		if re.search(r'(.+\,\s)', entity):
			#menghilangkan atribut setelah tanda koma
			entity = re.sub('\,.+', '', entity).strip()

		if re.search(r'(.+\,.+)', entity):
			entity = re.sub('\,.+', '', entity).strip()

		if re.match(r'.+\s(dan)\s.+', entity):
			parts = re.split(' dan ', entity, 1)
			left = parts[0]
			right = parts[1]
			arr_clean_entity.append(left + " | " + provinsi + " | " + kabupaten_kota)
			arr_clean_entity.append(right + " | " + provinsi + " | " + kabupaten_kota)

		#menghilangkan special character dan angka
		entity = re.sub('[^A-Za-z\-\s\,\.\;\:\'\&]+', '', entity)

		#menghilangkan tanda baca di akhir
		entity = re.sub('\.{1,}$', '', entity).strip()
		entity = re.sub('\,{1,}$', '', entity).strip()
		entity = re.sub('\;{1,}$', '', entity).strip()
		entity = re.sub('\:{1,}$', '', entity).strip()

		arr_clean_entity.append(entity.strip() + " | " + provinsi.strip() + " | " + kabupaten_kota.strip())
	return arr_clean_entity

def contain_punctuation_or_contain_digit(value):
    for c in value:
        if c in string.punctuation or c.isdigit():
            return True
    return False

def buildKamus():
	kamus = []
	kamus_aksara = ["aksara"]
	kamus_alat_musik = ["alat musik", "gamelan"]
	kamus_bahasa = ["bahasa"]
	kamus_benteng = ["benteng"]
	kamus_candi = ["candi"]
	kamus_cerita_rakyat = ["cerita rakyat", "sastra", "hikayat", "legenda"]
	kamus_istana = ["istana", "keraton", "kraton", "istano"]
	kamus_kerajaan = ["kerajaan", "kesultanan", "kepaksian", "kadipaten", "kasunanan"]
	kamus_kolam = ["kolam"]
	kamus_komunitas_adat = ["komunitas adat", "masyarakat adat"]
	kamus_kuil = ["kuil"]
	kamus_lagu = ["lagu"]
	kamus_makam = ["makam", "kuburan", "pemakaman", "wisata ziarah"]
	kamus_makanan = ["makanan", "minuman", "jajanan", "kue", "oleh-oleh", "masakan", "kuliner", "pangan"]
	kamus_masjid = ["masjid"]
	kamus_motif = ["motif", "kain", "tenun"]
	kamus_musik = ["seni musik", "karawitan"]
	kamus_naskah = ["naskah", "manuskrip", "kitab", "kidung", "serat", "babad", "suluk", "wawacan", "sanghyang", "kakawin"]
	kamus_ornamen = ["ornamen"]
	kamus_pakaian = ["pakaian", "busana", "baju"]
	kamus_patung = ["patung", "arca"]
	kamus_permainan = ["permainan"]
	kamus_petirtaan = ["petirtaan"]
	kamus_prasasti = ["prasasti"]
	kamus_pura = ["pura"]
	kamus_ritual = ["ritual", "upacara", "perayaan tradisional", "tolak bala", "hajatan", "selamatan", "penyembuhan", "perkawinan", "tradisi", "event budaya", "event tradisional", "pesta"]
	kamus_rumah = ["rumah adat", "rumah tradisional", "arsitektur"]
	kamus_seni_pertunjukkan = ["seni pertunjukkan", "wayang", "atraksi"]
	kamus_senjata = ["senjata"]
	kamus_situs = ["situs", "kawasan megalit"]
	kamus_suku = ["suku", "etnis", "penduduk asli"]
	kamus_tari = ["tari", "tarian", "tari-tarian"]
	kamus_tradisi_lisan = ["tradisi lisan", "seni berpantun", "seni vokal", "seni suara", "seni lisan", "sastra lisan"]
	kamus.append(kamus_aksara)
	kamus.append(kamus_alat_musik)
	kamus.append(kamus_bahasa)
	kamus.append(kamus_benteng)
	kamus.append(kamus_candi)
	kamus.append(kamus_cerita_rakyat)
	kamus.append(kamus_istana)
	kamus.append(kamus_kerajaan)
	kamus.append(kamus_kolam)
	kamus.append(kamus_komunitas_adat)
	kamus.append(kamus_kuil)
	kamus.append(kamus_lagu)
	kamus.append(kamus_makam)
	kamus.append(kamus_makanan)
	kamus.append(kamus_masjid)
	kamus.append(kamus_motif)
	kamus.append(kamus_musik)
	kamus.append(kamus_naskah)
	kamus.append(kamus_ornamen)
	kamus.append(kamus_pakaian)
	kamus.append(kamus_patung)
	kamus.append(kamus_permainan)
	kamus.append(kamus_petirtaan)
	kamus.append(kamus_prasasti)
	kamus.append(kamus_pura)
	kamus.append(kamus_ritual)
	kamus.append(kamus_rumah)
	kamus.append(kamus_seni_pertunjukkan)
	kamus.append(kamus_senjata)
	kamus.append(kamus_situs)
	kamus.append(kamus_suku)
	kamus.append(kamus_tari)
	kamus.append(kamus_tradisi_lisan)
	return kamus

#membentuk kamus budaya
kamus = buildKamus()
#model untuk melakukan tagging
# ct = CRFTagger()
# ct.set_model_file('all_indo_man_tag_corpus_model.crf.tagger')

#perhitungan statistik hasil ekstraksi masing-masing program
#digunakan untuk menghitung statistik hasil ekstraksi
def printStatisticTableKabupatenKota(typescrape, map_provinsi_kabupaten, kabupaten_kota_heritage):
	fname_statistik ='statistik_kabkota_{}.csv'.format(typescrape)
	f_statistik = codecs.open(fname_statistik, 'w')
	#header tabel statistik
	headerStr = "Provinsi - Kabupaten/Kota"
	for key, value in kabupaten_kota_heritage["Aceh"][map_provinsi_kabupaten["Aceh"][0]].iteritems():
		headerStr = headerStr + ',' + key
	headerStr = headerStr.replace('\n','')
	headerStr = headerStr.encode('utf-8')
	f_statistik.write(headerStr + '\n') 
	for k, v in map_provinsi_kabupaten.iteritems():
		for temp in v:
			rowStr = k + " - " + temp
			for key, value in kabupaten_kota_heritage[k][temp].iteritems():
				rowStr = rowStr + ',' + str(len(value))
			rowStr = rowStr.replace('\n','')
			rowStr = rowStr.encode('utf-8')
			f_statistik.write(rowStr + '\n')
	f_statistik.close()

#perhitungan statistik hasil ekstraksi masing-masing program
#digunakan untuk menghitung statistik hasil ekstraksi
def printStatisticTable(typescrape, array_nama_provinsi, provinsi_heritage):
	fname_statistik ='statistik_{}.csv'.format(typescrape)
	f_statistik = codecs.open(fname_statistik, 'w')
	#header tabel statistik
	headerStr = "Provinsi"
	for key, value in provinsi_heritage[array_nama_provinsi[0]].iteritems():
		headerStr = headerStr + ',' + key
	headerStr = headerStr.replace('\n','')
	headerStr = headerStr.encode('utf-8')
	f_statistik.write(headerStr + '\n') 
	for nama_provinsi in array_nama_provinsi:
		rowStr = nama_provinsi
		for key, value in provinsi_heritage[nama_provinsi].iteritems():
			rowStr = rowStr + ',' + str(len(value))
		rowStr = rowStr.replace('\n','')
		rowStr = rowStr.encode('utf-8')
		f_statistik.write(rowStr + '\n')
	f_statistik.close()

#digunakan untuk mendapatkan postag
def getPOSTag(token):
	return ct.tag_sents([[token]])
	# response = client.service.getPOSTag(token)
	# #mendapatkan root
	# root = ET.fromstring(response)
	# #iterate over children
	# element = root.findall('element')
	# postag = element[0].find('postag').text
	# return postag

#digunakan untuk mencetak hasil ekstraksi ke berkas dengan format.txt
def printToFileAllHeritageKabupatenKota(typescrape, kabupaten_kota_heritage, nama_provinsi, nama_kabupaten_kota):
	for key, value in kabupaten_kota_heritage[nama_provinsi][nama_kabupaten_kota].iteritems():
		foutputname = 'cultural_heritage_{}_{}_{}_{}.txt'.format(typescrape, key, nama_provinsi, nama_kabupaten_kota)
		foutput = open(foutputname, "w")
		for v in value:
			print (v, file=foutput)
		foutput.close()
		shutil.move(src + foutputname, dest_kab_kota + key + "/" + foutputname)

#digunakan untuk mencetak hasil ekstraksi ke berkas dengan format.txt
def printToFileAllHeritage(typescrape, provinsi_heritage, nama_provinsi):
	for key, value in provinsi_heritage[nama_provinsi].iteritems():
		foutputname = 'cultural_heritage_{}_{}_{}.txt'.format(typescrape, key, nama_provinsi)
		foutput = open(foutputname, "w")
		for v in value:
			print (v, file=foutput)
		foutput.close()
		shutil.move(src + foutputname, dest + key + "/" + foutputname)

def getAllSectionTitle(soup, map_index_baris):
	h2 = soup.find_previous("h2")
	h3 = soup.find_previous("h3")
	h4 = soup.find_previous("h4")
	h5 = soup.find_previous("h5")
	h6 = soup.find_previous("h6")
	h6_text = " "
	h5_text = " "
	h4_text = " "
	h3_text = " "
	h2_text = " "
	index_baris_h6 = -1
	index_baris_h5 = -1
	index_baris_h4 = -1
	index_baris_h3 = -1
	index_baris_h2 = -1
	judul_section = ""
	for key, value in map_index_baris.iteritems():
		if h6 is not None:
			if str(h6) in str(key):
				index_baris_h6 = value
		if h5 is not None:
			if str(h5) in str(key):
				index_baris_h5 = value
		if h4 is not None:
			if str(h4) in str(key):
				index_baris_h4 = value
		if h3 is not None:
			if str(h3) in str(key):
				index_baris_h3 = value
		if h2 is not None:
			if str(h2) in str(key):
				index_baris_h2 = value
	try:
		if index_baris_h6 > index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
			h6_text = h6.getText().replace('[sunting | sunting sumber]','').strip()
			h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
			h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
			h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
			h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
		else:
			if index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
				h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
				h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
				h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
			else:
				if index_baris_h4 > index_baris_h3 > index_baris_h2:
					h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
					h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
					h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				else:
					if index_baris_h3 > index_baris_h2:
						h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
						h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
					else:
						if index_baris_h2 != -1:
							h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
						else:
							print ("judul section tidak ditemukan hingga ke h2")	
		judul_section = h6_text + "\t" + h5_text + "\t" + h4_text + "\t" + h3_text + "\t" + h2_text
	except:
		print ("function getAllProperty - judul section berantakan")
	return judul_section


#digunakan untuk mengambil property
def getProperty(type, soup, map_index_baris):
	if type == "table":
		caption_text = ""
	judul_section = ""
	#mengambil judul section sebelumnya
	h2 = soup.find_previous("h2")
	h3 = soup.find_previous("h3")
	h4 = soup.find_previous("h4")
	h5 = soup.find_previous("h5")
	h6 = soup.find_previous("h6")
	h6_text = ""
	h5_text = ""
	h4_text = ""
	h3_text = ""
	h2_text = ""
	index_baris_h6 = -1
	index_baris_h5 = -1
	index_baris_h4 = -1
	index_baris_h3 = -1
	index_baris_h2 = -1
	judul_section = ""
	for key, value in map_index_baris.iteritems():
		if h6 is not None:
			if str(h6) in str(key):
				index_baris_h6 = value
		if h5 is not None:
			if str(h5) in str(key):
				index_baris_h5 = value
		if h4 is not None:
			if str(h4) in str(key):
				index_baris_h4 = value
		if h3 is not None:
			if str(h3) in str(key):
				index_baris_h3 = value
		if h2 is not None:
			if str(h2) in str(key):
				index_baris_h2 = value
	try:
		if index_baris_h6 > index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
			h6_text = h6.getText().replace('[sunting | sunting sumber]','').strip()
			h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
			h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
			h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
			h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
			judul_section = h6_text + " - " + h5_text + " - " + h4_text + " - " + h3_text + " - " + h2_text
		else:
			if index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
				h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
				h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
				h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				judul_section = h5_text + " - " + h4_text + " - " + h3_text + " - " + h2_text
			else:
				if index_baris_h4 > index_baris_h3 > index_baris_h2:
					h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
					h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
					h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
					judul_section = h4_text + " - " + h3_text + " - " + h2_text
				else:
					if index_baris_h3 > index_baris_h2:
						h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
						h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
						judul_section = h3_text + " - " + h2_text
					else:
						if index_baris_h2 != -1:
							h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
							judul_section = h2_text
						else:
							print ("property tidak ditemukan hingga ke h2")	
	except:
		print ("function getProperty - judul section berantakan")
	
	#mengambil caption tabel
	if type == "table":
		caption = soup.find("caption")
		if caption is not None: 
			caption_text = caption.getText()
	prop = ""
	#gabungkan properti juga ke dalam string entitas budaya
	if type == "table":
		prop = judul_section + " | " + caption_text
	elif type == "list":
		prop = judul_section
	elif type == "free_text":
		prop = judul_section
	return prop

#digunakan untuk mengisi map warisan budaya dengan entitas budaya
def fillProvinceHeritageMap(provinsi_heritage, nama_provinsi, array_all_heritage):
	provinsi_heritage[nama_provinsi]["aksara"] = list(array_all_heritage[0])
	provinsi_heritage[nama_provinsi]["alat_musik"] = list(array_all_heritage[1])
	provinsi_heritage[nama_provinsi]["bahasa"] = list(array_all_heritage[2])
	provinsi_heritage[nama_provinsi]["benteng"] = list(array_all_heritage[3])
	provinsi_heritage[nama_provinsi]["candi"] = list(array_all_heritage[4])
	provinsi_heritage[nama_provinsi]["cerita_rakyat"] = list(array_all_heritage[5])
	provinsi_heritage[nama_provinsi]["istana"] = list(array_all_heritage[6])
	provinsi_heritage[nama_provinsi]["kerajaan"] = list(array_all_heritage[7])
	provinsi_heritage[nama_provinsi]["kolam"] = list(array_all_heritage[8])
	provinsi_heritage[nama_provinsi]["komunitas_adat"] = list(array_all_heritage[9])
	provinsi_heritage[nama_provinsi]["kuil"] = list(array_all_heritage[10])
	provinsi_heritage[nama_provinsi]["lagu"] = list(array_all_heritage[11])
	provinsi_heritage[nama_provinsi]["makam"] = list(array_all_heritage[12])
	provinsi_heritage[nama_provinsi]["makanan"] = list(array_all_heritage[13])
	provinsi_heritage[nama_provinsi]["masjid"] = list(array_all_heritage[14])
	provinsi_heritage[nama_provinsi]["motif"] = list(array_all_heritage[15])
	provinsi_heritage[nama_provinsi]["musik"] = list(array_all_heritage[16])
	provinsi_heritage[nama_provinsi]["naskah"] = list(array_all_heritage[17])
	provinsi_heritage[nama_provinsi]["ornamen"] = list(array_all_heritage[18])
	provinsi_heritage[nama_provinsi]["pakaian"] = list(array_all_heritage[19])
	provinsi_heritage[nama_provinsi]["patung"] = list(array_all_heritage[20])
	provinsi_heritage[nama_provinsi]["permainan"] = list(array_all_heritage[21])
	provinsi_heritage[nama_provinsi]["petirtaan"] = list(array_all_heritage[22])
	provinsi_heritage[nama_provinsi]["prasasti"] = list(array_all_heritage[23])
	provinsi_heritage[nama_provinsi]["pura"] = list(array_all_heritage[24])
	provinsi_heritage[nama_provinsi]["ritual"] = list(array_all_heritage[25])
	provinsi_heritage[nama_provinsi]["rumah"] = list(array_all_heritage[26])
	provinsi_heritage[nama_provinsi]["seni_pertunjukkan"] = list(array_all_heritage[27])
	provinsi_heritage[nama_provinsi]["senjata"] = list(array_all_heritage[28])
	provinsi_heritage[nama_provinsi]["situs"] = list(array_all_heritage[29])
	provinsi_heritage[nama_provinsi]["suku"] = list(array_all_heritage[30])
	provinsi_heritage[nama_provinsi]["tari"] = list(array_all_heritage[31])
	provinsi_heritage[nama_provinsi]["tradisi_lisan"] = list(array_all_heritage[32])

def fillProvinceHeritageMapKabupatenKota(provinsi_heritage, nama_provinsi, nama_kabupaten_kota, array_all_heritage):
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["aksara"] = list(array_all_heritage[0])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["alat_musik"] = list(array_all_heritage[1])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["bahasa"] = list(array_all_heritage[2])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["benteng"] = list(array_all_heritage[3])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["candi"] = list(array_all_heritage[4])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["cerita_rakyat"] = list(array_all_heritage[5])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["istana"] = list(array_all_heritage[6])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["kerajaan"] = list(array_all_heritage[7])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["kolam"] = list(array_all_heritage[8])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["komunitas_adat"] = list(array_all_heritage[9])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["kuil"] = list(array_all_heritage[10])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["lagu"] = list(array_all_heritage[11])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["makam"] = list(array_all_heritage[12])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["makanan"] = list(array_all_heritage[13])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["masjid"] = list(array_all_heritage[14])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["motif"] = list(array_all_heritage[15])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["musik"] = list(array_all_heritage[16])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["naskah"] = list(array_all_heritage[17])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["ornamen"] = list(array_all_heritage[18])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["pakaian"] = list(array_all_heritage[19])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["patung"] = list(array_all_heritage[20])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["permainan"] = list(array_all_heritage[21])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["petirtaan"] = list(array_all_heritage[22])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["prasasti"] = list(array_all_heritage[23])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["pura"] = list(array_all_heritage[24])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["ritual"] = list(array_all_heritage[25])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["rumah"] = list(array_all_heritage[26])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["seni_pertunjukkan"] = list(array_all_heritage[27])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["senjata"] = list(array_all_heritage[28])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["situs"] = list(array_all_heritage[29])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["suku"] = list(array_all_heritage[30])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["tari"] = list(array_all_heritage[31])
	provinsi_heritage[nama_provinsi][nama_kabupaten_kota]["tradisi_lisan"] = list(array_all_heritage[32])

#kamus yang digunakan untuk mengambil entitas budaya
def getKategoriIndex(warisan_budaya):
	counter = 0
	for budaya in kamus:
		for kata in budaya:
			if kata in warisan_budaya:
				return counter
		counter = counter + 1
	return -1

def match_aksara(aksara):
	if re.match(r'^aksara\s(.)+', aksara, re.I):
		return True
	return False

def match_alat_musik(alat_musik):
	if re.match(r'^gamelan\s(.)+', alat_musik, re.I):
		return True
	return False

def match_bahasa(bahasa):
	if re.match(r'^bahasa\s(.)+', bahasa, re.I):
		return True
	return False

def match_benteng(benteng):
	if re.match(r'^benteng\s(.)+', benteng, re.I):
		return True
	return False

def match_candi(candi):
	if re.match(r'^candi\s(.)+', candi, re.I):
		return True
	return False

def match_cerita_rakyat(cerita_rakyat):
	match_hikayat = re.match(r'^hikayat\s(.)+', cerita_rakyat, re.I)
	match_legenda = re.match(r'^legenda\s(.)+', cerita_rakyat, re.I)
	if match_hikayat or match_legenda:
		return True
	return False

def match_istana(istana):
	match_istana = re.match(r'^istana\s(.)+', istana, re.I)
	match_kompleks_istana = re.match(r'^kompleks\sistana\s(.)+', istana, re.I)
	match_keraton = re.match(r'^keraton\s(.)+', istana, re.I)
	match_kraton = re.match(r'^kraton\s(.)+', istana, re.I)
	match_istano = re.match(r'^istano\s(.)+', istana, re.I)
	if match_istana or match_kompleks_istana or match_keraton or match_kraton or match_istano:
		return True
	return False

def match_kerajaan(kerajaan):
	match_kerajaan = re.match(r'^kerajaan\s(.)+', kerajaan, re.I)
	match_kesultanan = re.match(r'^kesultanan\s(.)+', kerajaan, re.I)
	match_kepaksian = re.match(r'^kepaksian\s(.)+', kerajaan, re.I)
	match_kadipaten = re.match(r'^kadipaten\s(.)+', kerajaan, re.I)
	match_kasunanan = re.match(r'^kasunanan\s(.)+', kerajaan, re.I)
	if match_kerajaan or match_kesultanan or match_kepaksian or match_kadipaten or match_kasunanan:
		return True
	return False

def match_kolam(kolam):
	if re.match(r'^kolam\s(.)+', kolam, re.I):
		return True
	return False

def match_komunitas_adat(komunitas_adat):
	match_komunitas_adat = re.match(r'^komunitas adat\s(.)+', komunitas_adat, re.I)
	match_masyarakat_adat = re.match(r'^masyarakat adat\s(.)+', komunitas_adat, re.I)
	if match_komunitas_adat or match_masyarakat_adat:
		return True
	return False

def match_kuil(kuil):
	if re.match(r'^kuil\s(.)+', kuil, re.I):
		return True
	return False

def match_lagu(lagu):
	if re.match(r'^lagu\s(.)+', lagu, re.I):
		return True
	return False

def match_makam(makam):
	match_makam = re.match(r'^makam\s(.)+', makam, re.I)
	match_komplek_makam = re.match(r'^komplek\smakam\s(.)+', makam, re.I)
	match_kompleks_makam = re.match(r'^kompleks\smakam\s(.)+', makam, re.I)
	match_kuburan = re.match(r'^kuburan\s(.)+', makam, re.I)
	match_pemakaman = re.match(r'^pemakaman\s(.)+', makam, re.I)
	if match_makam or match_komplek_makam or match_kompleks_makam or match_kuburan or match_pemakaman:
		return True
	return False

def match_makanan(makanan):
	if re.match(r'^kue\s(.)+', makanan, re.I):
		return True
	return False

def match_masjid(masjid):
	if re.match(r'^masjid\s(.)+', masjid, re.I):
		return True
	return False

def match_motif(motif):
	match_kain = re.match(r'^kain\s(.)+', motif, re.I)
	match_tenun = re.match(r'^tenun\s(.)+', motif, re.I)
	if match_kain or match_tenun:
		return True
	return False

def match_musik(musik):
	if re.match(r'^musik\s(.)+', musik, re.I):
		return True
	return False

def match_naskah(naskah):
	match_naskah = re.match(r'^naskah\s(.)+', naskah, re.I)
	match_babad = re.match(r'^babad\s(.)+', naskah, re.I)
	match_kidung = re.match(r'^kidung\s(.)+', naskah, re.I)
	match_serat = re.match(r'^serat\s(.)+', naskah, re.I)
	match_kitab = re.match(r'^kitab\s(.)+', naskah, re.I)
	match_suluk = re.match(r'^suluk\s(.)+', naskah, re.I)
	match_wawacan = re.match(r'^wawacan\s(.)+', naskah, re.I)
	match_sanghyang = re.match(r'^sanghyang\s(.)+', naskah, re.I)
	match_kakawin = re.match(r'^kakawin\s(.)+', naskah, re.I)
	match_pustaka = re.match(r'^pustaka\s(.)+', naskah, re.I)
	if match_naskah or match_babad or match_kidung or match_serat or match_kitab or match_suluk or match_wawacan or match_sanghyang or match_kakawin or match_pustaka:
		return True
	return False

def match_pakaian(pakaian):
	match_pakaian = re.match(r'^pakaian\s(.)+', pakaian, re.I)
	match_busana = re.match(r'^busana\s(.)+', pakaian, re.I)
	match_baju = re.match(r'^baju\s(.)+', pakaian, re.I)
	if match_pakaian or match_busana:
		return True
	return False

def match_patung(patung):
	match_patung = re.match(r'^patung\s(.)+', patung, re.I)
	match_arca = re.match(r'^arca\s(.)+', patung, re.I)
	if match_patung or match_arca:
		return True
	return False

def match_permainan(permainan):
	match_atraksi_permainan = re.match(r'^atraksi\spermainan\s(.)+', permainan, re.I)
	if match_atraksi_permainan:
		return True
	return False

def match_petirtaan(petirtaan):
	match_petirtaan = re.match(r'^petirtaan\s(.)+', petirtaan, re.I)
	if match_petirtaan:
		return True
	return False

def match_prasasti(prasasti):
	if re.match(r'^prasasti\s(.)+', prasasti, re.I):
		return True
	return False

def match_pura(pura):
	if re.match(r'^pura\s(.)+', pura, re.I):
		return True
	return False

def match_rumah(rumah):
	match_rumah_adat = re.match(r'^rumah\sadat\s(.)+', rumah, re.I)
	match_rumah_tradisional = re.match(r'^rumah\stradisional\s(.)+', rumah, re.I)
	match_arsitektur = re.match(r'^arsitektur\s(.)+', rumah, re.I)
	if match_rumah_adat or match_rumah_tradisional or match_arsitektur:
		return True
	return False

def match_seni_pertunjukkan(seni_pertunjukkan):
	match_wayang = re.match(r'^wayang\s(.)+', seni_pertunjukkan, re.I)
	if match_wayang:
		return True
	return False

def match_ritual(ritual):
	match_ritual = re.match(r'^pesta\s(.)+', ritual, re.I)
	match_upacara = re.match(r'^upacara\s(.)+', ritual, re.I)
	if match_ritual or match_upacara:
		return True
	return False

def match_situs(situs):
	match_situs = re.match(r'^situs\s(.)+', situs, re.I)
	match_kawasan_megalit = re.match(r'^kawasan megalit\s(.)+', situs, re.I)
	if match_situs:
		filter_situs = re.match(r'^((?!web).)*$', situs, re.I)
		if filter_situs:
			return True
	if match_kawasan_megalit:
		return True
	return False

def match_suku(suku):
	match_suku = re.match(r'^suku\s(.)+', suku, re.I)
	match_etnis = re.match(r'^etnis\s(.)+', suku, re.I)
	if match_suku or match_etnis:
		filter_suku = re.match(r'^((?!bangsa).)*$', suku, re.I)
		if filter_suku:
			return True
	return False

def match_tari(tari):
	match_tari = re.match(r'^tari\s(.)+', tari, re.I)
	match_tarian = re.match(r'^tarian\s(.)+', tari, re.I)
	if match_tari or match_tarian:
		return True
	return False

def match_awalan(warisan_budaya):
	if match_aksara(warisan_budaya):
		return 0
	elif match_alat_musik(warisan_budaya):
		return 1
	elif match_bahasa(warisan_budaya):
		return 2
	elif match_benteng(warisan_budaya):
		return 3
	elif match_candi(warisan_budaya):
		return 4
	elif match_cerita_rakyat(warisan_budaya):
		return 5
	elif match_istana(warisan_budaya):
		return 6
	elif match_kerajaan(warisan_budaya):
		return 7
	elif match_kolam(warisan_budaya):
		return 8
	elif match_komunitas_adat(warisan_budaya):
		return 9
	elif match_kuil(warisan_budaya):
		return 10
	elif match_lagu(warisan_budaya):
		return 11
	elif match_makam(warisan_budaya):
		return 12
	elif match_makanan(warisan_budaya):
		return 13
	elif match_masjid(warisan_budaya):
		return 14
	elif match_motif(warisan_budaya):
		return 15
	elif match_musik(warisan_budaya):
		return 16
	elif match_naskah(warisan_budaya):
		return 17
	elif match_pakaian(warisan_budaya):
		return 19
	elif match_patung(warisan_budaya):
		return 20
	elif match_permainan(warisan_budaya):
		return 21
	elif match_petirtaan(warisan_budaya):
		return 22
	elif match_prasasti(warisan_budaya):
		return 23
	elif match_pura(warisan_budaya):
		return 24
	elif match_ritual(warisan_budaya):
		return 25
	elif match_rumah(warisan_budaya):
		return 26
	elif match_seni_pertunjukkan(warisan_budaya):
		return 27	
	elif match_situs(warisan_budaya):
		return 29
	elif match_suku(warisan_budaya):
		return 30
	elif match_tari(warisan_budaya):
		return 31
	else:
		return -1

def make_empty(arrayofallheritage):
	for array in arrayofallheritage:
		array[:] = []

def make_array():
	array_aksara = []
	array_alat_musik = []
	array_bahasa = []
	array_benteng = []
	array_candi = []
	array_cerita_rakyat = []
	array_istana = []
	array_kerajaan = []
	array_kolam = []
	array_komunitas_adat = []
	array_kuil = []
	array_lagu = []
	array_makam = []
	array_makanan = []
	array_masjid = []
	array_motif = []
	array_musik = []
	array_naskah = []
	array_ornamen = []
	array_pakaian = []
	array_patung = []
	array_permainan = []
	array_petirtaan = []
	array_prasasti = []
	array_pura = []
	array_ritual = []
	array_rumah = []
	array_seni_pertunjukkan = []
	array_senjata = []
	array_situs = []
	array_suku = []
	array_tari = []
	array_tradisi_lisan = []
	array_all_heritage = []
	array_all_heritage.append(array_aksara)
	array_all_heritage.append(array_alat_musik) 
	array_all_heritage.append(array_bahasa)
	array_all_heritage.append(array_benteng) 
	array_all_heritage.append(array_candi) 
	array_all_heritage.append(array_cerita_rakyat) 
	array_all_heritage.append(array_istana) 
	array_all_heritage.append(array_kerajaan) 
	array_all_heritage.append(array_kolam)
	array_all_heritage.append(array_komunitas_adat)
	array_all_heritage.append(array_kuil) 
	array_all_heritage.append(array_lagu) 
	array_all_heritage.append(array_makam) 
	array_all_heritage.append(array_makanan)
	array_all_heritage.append(array_masjid)
	array_all_heritage.append(array_motif)  
	array_all_heritage.append(array_musik) 
	array_all_heritage.append(array_naskah) 
	array_all_heritage.append(array_ornamen) 
	array_all_heritage.append(array_pakaian) 
	array_all_heritage.append(array_patung)
	array_all_heritage.append(array_permainan) 
	array_all_heritage.append(array_petirtaan)
	array_all_heritage.append(array_prasasti) 
	array_all_heritage.append(array_pura) 
	array_all_heritage.append(array_ritual) 
	array_all_heritage.append(array_rumah) 
	array_all_heritage.append(array_seni_pertunjukkan)
	array_all_heritage.append(array_senjata)
	array_all_heritage.append(array_situs) 
	array_all_heritage.append(array_suku) 
	array_all_heritage.append(array_tari) 
	array_all_heritage.append(array_tradisi_lisan)
	return array_all_heritage