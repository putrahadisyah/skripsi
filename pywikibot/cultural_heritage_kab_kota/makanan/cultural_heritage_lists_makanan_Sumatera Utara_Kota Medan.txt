Merdeka Walk, pusat jajanan 24 jam yang terletak di Lapangan Merdeka Medan dan tepat berada di seberang Balai Kota Lama Medan. | Wisata kuliner - Situs pariwisata
Ramadhan Fair, khusus dibuka pada saat bulan Ramadhan terletak bersebelahan dengan Masjid Raya Medan. | Wisata kuliner - Situs pariwisata
Kuliner Pagaruyung, masakan India dan Indonesia di daerah "Kampung Keling" ("Kampung Madras"). | Wisata kuliner - Situs pariwisata
Asia Mega Mas Food Court Centre 唐 人 街, Terletak di Kompleks Asia Mega Mas Medan. | Wisata kuliner - Situs pariwisata
Pasar Merah Square, terletak di Jalan H.M. Jhoni, berdekatan dengan Kampus ITM dan UMSU. | Wisata kuliner - Situs pariwisata
Amaliun Food Court, terletak di Jalan Amaliun, dekat dengan Yuki Simpang Raya. | Wisata kuliner - Situs pariwisata
Jalan Dr. Mansyur (Kampus USU), pilihan berbagai cafe yang menawarkan beragam hidangan. | Wisata kuliner - Situs pariwisata
Jalan Semarang, masakan Tionghoa pada malam hari. | Wisata kuliner - Situs pariwisata
Restoran Tip Top, Restoran yang dibangun pada zaman kolonial Belanda, terletak di Kesawan. | Wisata kuliner - Situs pariwisata
Imlek Fair, khusus diadakan menjelang perayaan Tahun Baru Imlek setahun sekali. | Wisata kuliner - Situs pariwisata
