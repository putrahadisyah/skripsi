Kare Rajungan | Masakan - Kuliner Khas Tuban
Becek Menthok | Masakan - Kuliner Khas Tuban
Belut Pedas | Masakan - Kuliner Khas Tuban
Sate Bebek | Masakan - Kuliner Khas Tuban
Asem-asem Ndas Manyung | Masakan - Kuliner Khas Tuban
Mangut Pee | Masakan - Kuliner Khas Tuban
Jangan Kelor + Nasi Jagung + Petis + Ikan Pindang | Masakan - Kuliner Khas Tuban
Legen | Minuman - Kuliner Khas Tuban
Tuak/Arak alias Saguer | Minuman - Kuliner Khas Tuban
Es Dawet Siwalan | Minuman - Kuliner Khas Tuban
Es Degan | Minuman - Kuliner Khas Tuban
Angsle | Minuman - Kuliner Khas Tuban
Ampo | Oleh-oleh - Kuliner Khas Tuban
Keripik Gayam | Oleh-oleh - Kuliner Khas Tuban
Kecap Laron | Oleh-oleh - Kuliner Khas Tuban
Keripik Ikan | Oleh-oleh - Kuliner Khas Tuban
Miniatur Ongkek | Oleh-oleh - Kuliner Khas Tuban
Batik Gedog | Oleh-oleh - Kuliner Khas Tuban
