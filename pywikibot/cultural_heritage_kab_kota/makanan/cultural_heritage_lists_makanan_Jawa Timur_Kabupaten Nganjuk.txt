Nasi becek, sejenis gulai kambing yang memiliki rasa khas dengan penambahan irisan daun jeruk nipis. | Makanan
Dumbleg, sejenis dodol yang terbuat dari ketan. Makanan ini hanya ada pada hari-hari tertentu di Pasar Gondang (tiap Pasaran Pon) dan Pasar Rejoso (tiap pasaran kliwon). | Makanan
Onde-onde Njeblos, semacam onde-onde tetapi tidak berisi. Berbentuk seperti bola yang ditaburi wijen. | Makanan
Nasi Pecel: menu nasi dengan sayur (kulup) kangkung, toge, kacang panjang, kembang turi dll disiram dengan kuah sambal kacang dengan ciri khas pedas dan disertai tempe, tahu goreng serta rempeyek yang renyah. | Makanan
Nasi Tumpang, seperti halnya nasi pecel namun ada menu tambahan berupa sayur (sambal) tumpang, yg terbuat dari tempe "busuk" (tempe difermentasikan) yang dimasak dengan bumbu lain yang rasanya gurih dan pedas. | Makanan
Kerupuk Upil, adalah kerupuk kecil yang digoreng tanpa minyak tetapi menggunakan pasir | Makanan
Tepo Mbah Umbruk, seperti lontong bungkusnya dari daun pisang bentuknya kerucut dan agak miring dengan sayur kacang panjang tetapi di ambil isinya atau disebut kacang tolo dan bumbu dan bahan bahan lain. Sampai saat ini pun, Tepo Mbah Umbruk bisa dinikmati. | Makanan
Kerupuk pecel adalah kerupuk bakar yang dicampur dengan sayuran,yang terdiri dari capar (toge), bayam, bung (rebung), kenikir, mbayung (daun kacang) dan kacang panjang yang kemudian di siram dengan bumbu pecel dan minumnya adalah es rujak. | Makanan
