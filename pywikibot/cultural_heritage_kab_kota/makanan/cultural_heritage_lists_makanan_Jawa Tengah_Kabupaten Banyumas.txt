Mendoan | Masakan - Kuliner khas Banyumas
Sate Bebek Tambak | Masakan - Kuliner khas Banyumas
Soto Sokaraja | Masakan - Kuliner khas Banyumas
Gethuk Goreng Sokaraja | Masakan - Kuliner khas Banyumas
Es Dawet Banyumas | Minuman - Kuliner khas Banyumas
Wedang Runtah | Minuman - Kuliner khas Banyumas
Getuk goreng sokaraja | Jajanan - Kuliner khas Banyumas
Jenang jaket khas Mersi (Purwokerto Timur) | Jajanan - Kuliner khas Banyumas
Kraca | Jajanan - Kuliner khas Banyumas
Keripik tempe | Jajanan - Kuliner khas Banyumas
Kue Gelombang Samudra | Jajanan - Kuliner khas Banyumas
Nopia | Oleh-oleh - Kuliner khas Banyumas
Mino (Mini Nopia) | Oleh-oleh - Kuliner khas Banyumas
