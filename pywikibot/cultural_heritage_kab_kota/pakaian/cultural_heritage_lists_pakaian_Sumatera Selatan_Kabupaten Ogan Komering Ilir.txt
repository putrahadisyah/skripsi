Angkinan: Baju pengantin/baju kebesaran adat Kayuagung | Pakaian Adat - Wisata Warisan Budaya Daerah
Kebaya Kurung Panjang: ciri yang memakai sudah bersuami | Pakaian Adat - Wisata Warisan Budaya Daerah
Kebaya Kurung Pendek/bunting: cirri yang memakai masih perawan | Pakaian Adat - Wisata Warisan Budaya Daerah
Kebaya Tapuk: Ciri yang memakai sudah bersuami | Pakaian Adat - Wisata Warisan Budaya Daerah
Kebaya Tojang: untuk undangan kehormatan/misal si ibu pengantin lakilaki diundang menghadiri hidangan atau kedulangan atau untuk menghadiri pernikahan | Pakaian Adat - Wisata Warisan Budaya Daerah
Balah Buluh: Pakaian laki-laki yang dilengkapi dengan Kepudang atau kopiah (kain berada di luar baju) | Pakaian Adat - Wisata Warisan Budaya Daerah
Teluk Belango: sejenis baju untuk kaum laki-laki untuk kepentingan adat dengan memakai peci dan kain dibalik baju | Pakaian Adat - Wisata Warisan Budaya Daerah
Sarung Pelikat:bentuk kain untuk lakilaki yang terbuat dari jerat jerami yang bermotif kotak-kotak besar ataupun kecil | Pakaian Adat - Wisata Warisan Budaya Daerah
Sarung bugis: untuk laki-laki | Pakaian Adat - Wisata Warisan Budaya Daerah
Kain Putungan (kain panjang) untuk pasangan kebaya pendek maupun kurung maupun kebaya biasa | Pakaian Adat - Wisata Warisan Budaya Daerah
Sarung Sungkitan (songket): pasangan Angkinan juga bisa untuk kebaya biasa | Pakaian Adat - Wisata Warisan Budaya Daerah
