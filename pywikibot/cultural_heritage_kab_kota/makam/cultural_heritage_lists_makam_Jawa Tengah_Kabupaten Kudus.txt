Makam Sunan Kudus, di Desa Kauman | Wisata religi (ziarah) - Pariwisata
Makam Sunan Muria, di Desa Colo | Wisata religi (ziarah) - Pariwisata
Makam Sunan Kedu, (dia berasal dari daerah Kedu dan menjadi murid Sunan Kudus), di Desa Gribig | Wisata religi (ziarah) - Pariwisata
Makam Syeh Syadzili (dekat Air Tiga Rasa), di Desa Japan | Wisata religi (ziarah) - Pariwisata
Makam Kyai Telingsing (Merupakan guru Sunan Kudus dan sesepuh dari Kota Kudus yang berasal dari China dengan nama asli The Ling Sing), di Desa Sunggingan | Wisata religi (ziarah) - Pariwisata
Makam Keluarga Trah Tjondronegoro III dan Keluarga Besar R.A. Kartini, di Desa Kaliputu | Wisata religi (ziarah) - Pariwisata
Makam Mbah Tanggulangin, di Dukuh Plenyian Desa Demaan | Wisata religi (ziarah) - Pariwisata
Makam Sedo Mukti, di Desa Kaliputu | Wisata religi (ziarah) - Pariwisata
Makam Sosro Kartono & para Bupati, di Desa Kaliputu | Wisata religi (ziarah) - Pariwisata
